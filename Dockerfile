FROM ubuntu:20.04

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND=noninteractive
ENV ASDF_VERSION=0.11.3
ENV ERLANG_VERSION=26.0.1
ENV ELIXIR_VERSION=1.14.5-otp-26
ENV LANG=C.UTF-8

RUN apt-get update -q && apt-get install -y git curl

RUN git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v${ASDF_VERSION}

ENV PATH=/root/.asdf/bin:/root/.asdf/shims:$PATH

RUN echo '. "$HOME/.asdf/asdf.sh"' >> ~/.bashrc

# Erlang dependencies
RUN apt-get -y install \
  build-essential \
  autoconf \
  m4 \
  libncurses5-dev \
  libssh-dev \
  libncurses-dev \
  openjdk-17-jdk

RUN asdf plugin add erlang https://github.com/asdf-vm/asdf-erlang.git
RUN asdf install erlang ${ERLANG_VERSION}
RUN asdf global erlang ${ERLANG_VERSION}

# Elixir build dependencies
RUN apt-get -y install unzip

RUN asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
RUN asdf install elixir ${ELIXIR_VERSION}
RUN asdf global elixir ${ELIXIR_VERSION}

RUN mix local.hex --force && mix local.rebar --force

WORKDIR /app
COPY . .

RUN echo $PATH > /tmp/debug.log
ENV MIX_ENV=prod
RUN mix deps.get
RUN mix release
