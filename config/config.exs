import Config

config :bridge,
  minecraft_path: "."

config :nadia,
  token: "bot_token"

import_config "#{config_env()}.exs"
