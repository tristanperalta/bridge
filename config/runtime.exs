import Config

config :nadia,
  token: System.get_env("TELEGRAM_BOT_TOKEN")
