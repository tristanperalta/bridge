defmodule Bridge.LogParser do
  defstruct [:time, :module, :event, :message]

  def parse(log_message) do
    case Regex.named_captures(~r/\[(?<time>.*)\] \[(?<module>.*)\]: (?<message>.*$)/, log_message) do
      %{"time" => time, "module" => module, "message" => message} ->
        time =
          case Time.from_iso8601(time) do
            {:ok, time} -> time
            _ -> nil
          end

        %__MODULE__{time: time, module: module, message: message}

      _ ->
        nil
    end
  end

  def parse_event(%__MODULE__{message: message} = event) do
    patterns = [
      online: ~r/^Done .*$/,
      joined: ~r/joined the game/,
      left: ~r/left the game/,
      saving: ~r/^Saving the game/,
      saved: ~r/^Saved the game/,
      auto_save_disabled: ~r/^Automatic saving is now disabled/,
      auto_save_enabled: ~r/^Automatic saving is now enabled/,
      stopping: ~r/^Stopping server/
    ]

    Enum.reduce_while(patterns, nil, fn {event, pattern}, acc ->
      if String.match?(message, pattern) do
        {:halt, event}
      else
        {:cont, acc}
      end
    end)
    |> then(&Map.put(event, :event, &1))
  end

  def parse_event(_), do: %__MODULE__{}
end
