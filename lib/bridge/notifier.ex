defmodule Bridge.Notifier do
  use GenServer

  alias Bridge.LogParser

  def start_link(file) do
    GenServer.start_link(__MODULE__, [file: file], name: __MODULE__)
  end

  def init(opts) do
    file = Keyword.get(opts, :file)
    pid = Port.open({:spawn, "tail -f #{file}"}, [:binary])
    IO.puts("Initializing notifier")
    {:ok, %{pid: pid}}
  end

  def handle_info({_port, {:data, data}}, state) do
    data
    |> LogParser.parse()
    |> LogParser.parse_event()
    |> send_notif()

    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  def send_notif(%{event: event_type, message: message})
      when event_type in [:joined, :left] do
    Nadia.send_message(chat_id(), message)
  end

  def send_notif(%{event: :online}) do
    Nadia.send_message(chat_id(), "Minecraft Server is online.")
  end

  def send_notif(_), do: nil

  defp chat_id do
    Application.fetch_env!(:bridge, :chat_id)
  end
end
