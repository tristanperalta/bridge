defmodule Bridge.Server do
  use GenServer

  require Logger

  @world_name "world"
  @backup_path "priv"
  @backup_interval 3_600 * 1_000 # every one hour in milliseconds

  alias Bridge.LogParser

  def start_link(opts) do
    GenServer.start_link( __MODULE__, opts, name: __MODULE__)
  end

  def init(opts) do
    minecraft_path = Keyword.get(opts, :minecraft_path, "./minecraft")
    backup_interval = Keyword.get(opts, :backup_interval, @backup_interval)
    backup_path = Keyword.get(opts, :backup_path, @backup_path)

    port =
      Port.open(
        {:spawn, "java -Xmx2024M -Xms2024M -jar server.jar -nogui"},
        [:binary, :exit_status, {:cd, minecraft_path}]
      )

    {:ok, %{
      minecraft_path: minecraft_path, 
      backup_interval: backup_interval,
      backup_path: backup_path,
      backup_state: :idle,
      port: port
    } }
  end

  def help() do
    GenServer.call(__MODULE__, :help)
  end

  def stop() do
    GenServer.call(__MODULE__, :stop)
  end

  def save_all() do
    GenServer.call(__MODULE__, :save_all)
  end

  def save_off() do
    GenServer.call(__MODULE__, :save_off)
  end

  def save_on() do
    GenServer.call(__MODULE__, :save_on)
  end

  def backup() do
    GenServer.call(__MODULE__, :backup)
  end

  def handle_continue(:backup, %{port: port, backup_state: :ongoing} = state) do
    Port.command(port, "save-all flush\n")
    {:noreply, state}
  end
  
  def handle_continue(_, state), do: {:noreply, state}

  def handle_info({_port, {:data, data}}, state) do
    data
    |> LogParser.parse()
    |> LogParser.parse_event()
    |> Map.pop(:event)
    |> then(fn {type, payload} -> handle_event(type, payload, state) end)

    Logger.info(data)
    {:noreply, state}
  end

  def handle_info({_port, {:exit_status, 0}}, state) do
    {:stop, :normal, state}
  end

  def handle_info(:inc_backup, %{backup_interval: backup_interval} = state) do
    Process.send_after(self(), :inc_backup, backup_interval)
    Process.send(self(), :backup, [:noconnect])
    {:noreply, state}
  end

  def handle_info(:backup, state) do
    Logger.info("Backup starting")
    {:noreply, Map.put(state, :backup_state, :ongoing), {:continue, :backup}}
  end

  def handle_info(:backup_done, state) do
    Logger.info("Backup done.")
    {:noreply, Map.put(state, :backup_state, :idle)}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  def handle_call(:help, _, %{port: port} = state) do
    Port.command(port, "help\n")
    {:reply, state, state}
  end

  def handle_call(:stop, _, %{port: port} = state) do
    Port.command(port, "stop\n")
    {:reply, state, state}
  end

  def handle_call(:save_all, _, %{port: port} = state) do
    Port.command(port, "save-all flush\n")
    {:reply, "save-all", state}
  end

  def handle_call(:save_off, _, %{port: port} = state) do
    Port.command(port, "save-off\n")
    {:reply, "save-off", state}
  end

  def handle_call(:save_on, _, %{port: port} = state) do
    Port.command(port, "save-on\n")
    {:reply, "save-on", state}
  end

  def handle_call(:backup, _, state) do
    Logger.info("Backup starting")
    {:reply, "backup starting", Map.put(state, :backup_state, :ongoing), {:continue, :backup}}
  end

  defp handle_event(event_type, %{message: message}, _state)
       when event_type in [:joined, :left] do
    Nadia.send_message(chat_id(), message)
  end

  defp handle_event(:online, _, _state) do
    # start incremental backup
    Process.send(self(), :inc_backup, [:noconnect])

    Nadia.send_message(chat_id(), "Minecraft Server is online.")
  end

  defp handle_event(:saved, _, %{port: port, backup_state: :ongoing}) do
    Port.command(port, "save-off\n")
  end

  defp handle_event(:auto_save_disabled, _, %{port: port, minecraft_path: minecraft_path, backup_state: :ongoing}) do
    do_backup(%{minecraft_path: minecraft_path})

    Port.command(port, "save-on\n")
  end

  defp handle_event(:auto_save_enabled, _, %{backup_state: :ongoing}) do
    Process.send(self(), :backup_done, [:noconnect])
  end

  defp handle_event(_event, _payload, _state), do: nil

  defp chat_id do
    Application.fetch_env!(:bridge, :chat_id)
  end

  defp do_backup(%{minecraft_path: minecraft_path}) do
    Process.sleep(500)

    now = DateTime.to_unix(DateTime.utc_now())

    Logger.info("Archiving world to disk.")
    System.cmd("tar", [
      "czf",
      "#{@backup_path}/#{@world_name}-#{now}.tar.gz",
      "-C",
      minecraft_path,
      "world"
    ])
    Logger.info("Archiving world to disk. Done.")
  end
end
