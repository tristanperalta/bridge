defmodule Bridge.LogParserTest do
  use ExUnit.Case

  alias Bridge.LogParser

  test "parses the log message" do
    message = "[07:43:16] [Server thread/INFO]: Done (4.190s)! For help, type \"help\""

    assert %{message: message} =
             LogParser.parse(message)
             |> LogParser.parse_event()

    assert message == "Done (4.190s)! For help, type \"help\""
  end

  test "parses joined event" do
    message = "[09:37:05] [Server thread/INFO]: t12a0622 joined the game"
    assert %{event: :joined} = LogParser.parse(message) |> LogParser.parse_event() |> IO.inspect()
  end

  test "parses left event" do
    message = "[11:41:21] [Server thread/INFO]: hailyay1211 left the game"
    assert %{event: :left} = LogParser.parse(message) |> LogParser.parse_event() |> IO.inspect()
  end
end
